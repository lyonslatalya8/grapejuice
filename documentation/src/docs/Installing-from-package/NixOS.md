title: Install Grapejuice on NixOS
---
## Installing Grapejuice

In the terminal, run the following command:

```sh
nix-env -iA nixpkgs.grapejuice
```

## Installing a patched Wine build

It's recommended that you install a patched version of Wine. See [this guide](../Guides/Installing-Wine)
for more information.
